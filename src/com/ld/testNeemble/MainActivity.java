package com.ld.testNeemble;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.view.animation.DecelerateInterpolator;

public class MainActivity extends Activity implements FlingGestureDetector.FlingCallback, View.OnTouchListener {

    public static final String PARAM_VIEW_C_VISIBLE = "param_view_c_visible";

    private static boolean DEBUG = true;

    private int mScreenLeftViewFactor;
    private View mRoot;
    private ViewGroup mViewB;
    private ViewGroup mViewC;
    private View mSlider;

    private int mScreenWidth;
    private int mXDelta;
    private int mLastX;
    private boolean mOpening;

    private Animator mLastAnimator;
    private AnimatorListener mLastAnimatorListener;
    private static final TimeInterpolator INTERPOLATOR = new DecelerateInterpolator(1.75f);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();
        mScreenLeftViewFactor = getResources().getInteger(R.integer.left_view_screen_factor);

        setContentView(R.layout.main);
        mRoot = findViewById(R.id.root);
        mViewB = (ViewGroup) findViewById(R.id.view_b);
        mViewC = (ViewGroup) findViewById(R.id.view_c);
        mSlider = findViewById(R.id.slider);

        applyParams(savedInstanceState);

        final FlingGestureDetector flingGestureDetector = new FlingGestureDetector(this);

        final GestureDetector gestureDetector = new GestureDetector(this, flingGestureDetector);
        final View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        };
        mViewC.setOnTouchListener(onTouchListener);
        mSlider.setOnTouchListener(this);
        mScreenWidth = getScreenWidth();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(PARAM_VIEW_C_VISIBLE, mViewB.getVisibility() == View.VISIBLE);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        applyParams(savedInstanceState);
    }

    private void applyParams(Bundle savedInstanceState) {
        boolean visibilityC = false;
        if (savedInstanceState != null) {
            visibilityC = savedInstanceState.getBoolean(PARAM_VIEW_C_VISIBLE);
        }
        if (!visibilityC) {
            mViewB.setVisibility(View.GONE);
            mViewC.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLeftSwipe() {
        animateOpen();
    }

    @Override
    public void onRightSwipe() {
        animateClose();
    }

    private static final String PROP_LEFT_PANE_ALPHA = "leftPaneAlpha";
    private static final String PROP_LEFT_PANE_LEFT = "leftPane";
    private static final String PROP_RIGHT_PANE_RIGHT = "rightPane";

    @SuppressWarnings("unused")
    public void setLeftPane(float value) {
        mViewB.setTranslationX(value);
        mViewB.requestLayout();
    }

    @SuppressWarnings("unused")
    public void setLeftPaneAlpha(float value) {
        mViewB.setAlpha(value);
        mViewB.requestLayout();
    }

    @SuppressWarnings("unused")
    public void setRightPane(float value) {
        mViewC.setTranslationX(value);
        mViewC.requestLayout();
    }

    private void animateOpen() {
        final AnimatorListener listener = new AnimatorListener(new View[]{mRoot, mViewB, mViewC}, null);
        final float alpha = mViewB.getAlpha();
        startLayoutAnimation(getResources().getInteger(R.integer.anim_time_full), listener,
                PropertyValuesHolder.ofFloat(PROP_LEFT_PANE_LEFT, mViewB.getX(), 0),
                PropertyValuesHolder.ofFloat(PROP_RIGHT_PANE_RIGHT, mViewC.getX() - mScreenWidth / mScreenLeftViewFactor, 0),
                PropertyValuesHolder.ofFloat(PROP_LEFT_PANE_ALPHA, alpha == 1f ? 0f : alpha, 1f));
    }

    private void animateClose() {
        final Display display = getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        final int width = size.x;
        float sliderX = mSlider.getX();
        if (DEBUG) {
            Log.d("animateClose", "right: from " + mViewC.getX() + " to " + sliderX);
        }
        final AnimatorListener listener = new AnimatorListener(null, new View[]{mViewB, mViewC});
        startLayoutAnimation(getResources().getInteger(R.integer.anim_time_full), listener,
                PropertyValuesHolder.ofFloat(PROP_LEFT_PANE_LEFT, mViewB.getX(), -1 * width),
                PropertyValuesHolder.ofFloat(PROP_RIGHT_PANE_RIGHT, mViewC.getX(), sliderX),
                PropertyValuesHolder.ofFloat(PROP_LEFT_PANE_ALPHA, mViewB.getAlpha(), 0f));
    }


    private void startLayoutAnimation(int duration, AnimatorListener listener, PropertyValuesHolder... values) {
        if (mLastAnimator != null) {
            mLastAnimator.cancel();
        }
        if (mLastAnimatorListener != null) {
            mLastAnimatorListener.cancel();
        }

        final ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(this, values).setDuration(duration);
        animator.setInterpolator(INTERPOLATOR);
        if (listener != null) {
            animator.addListener(listener);
        }
        mLastAnimator = animator;
        mLastAnimatorListener = listener;
        animator.start();
    }

    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                mXDelta = (int) (event.getRawX() - mSlider.getX());
                mViewB.setAlpha(0.0f);
            }
            break;
            case MotionEvent.ACTION_UP:
                if (mOpening) {
                    animateOpen();
                } else {
                    animateClose();
                }
                mLastX = mScreenWidth;
                mXDelta = 0;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                final int x = (int) event.getRawX() - mXDelta;
                final float leftPaneWidth = mScreenWidth / mScreenLeftViewFactor;
                final float rightPaneStartPoint = mSlider.getX();
                if (mViewB.getVisibility() != View.VISIBLE) {
                    mViewB.setX(-1 * leftPaneWidth);
                    mViewC.setX(rightPaneStartPoint);
                    mViewB.setVisibility(View.VISIBLE);
                    mViewC.setVisibility(View.VISIBLE);
                }
                if (x > rightPaneStartPoint) {
                    if (DEBUG) {
                        Log.d("onTouch", "1x = " + x + ", right = " + rightPaneStartPoint);
                    }
                    mViewC.setX(rightPaneStartPoint);
                } else {
                    if (DEBUG) {
                        Log.d("onTouch", "2x = " + x + ", right = " + (x >= leftPaneWidth ? x : leftPaneWidth));
                    }
                    mViewC.setX(x >= leftPaneWidth ? x : leftPaneWidth);
                }
                float factor = x <= rightPaneStartPoint ? getMovingFactor(x) : 0;
                final float leftX = leftPaneWidth * factor - leftPaneWidth;
                mViewB.setX(leftX <= 0 ? leftX : 0);
                mViewB.setAlpha(factor);
                mOpening = x <= mLastX;
                mLastX = x;
                break;
        }
        mRoot.invalidate();
        return true;
    }

    private int getScreenWidth() {
        final Display display = getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    private float getMovingFactor(float x) {
        float fullPath = mSlider.getX() - mScreenWidth / mScreenLeftViewFactor;
        return Math.abs(mSlider.getX() - x) / fullPath;
    }
}
