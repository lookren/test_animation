package com.ld.testNeemble;

import android.animation.Animator;
import android.view.View;

class AnimatorListener implements Animator.AnimatorListener {
    private final View[] mViewVisible;
    private final View[] mViewInvisible;

    private boolean mCancelled;

    public AnimatorListener(View[] viewVisible, View[] viewInvisible) {
        mViewVisible = viewVisible;
        mViewInvisible = viewInvisible;
    }

    public void cancel() {
        mCancelled = true;
    }

    @Override
    public void onAnimationStart(Animator animation) {
        if (mViewVisible != null) {
            for (View eachVisible : mViewVisible) {
                eachVisible.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }

    @Override
    public void onAnimationCancel(Animator animation) {
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        if (mCancelled) {
            return;
        }
        if (mViewInvisible != null) {
            for (View eachInvisible : mViewInvisible) {
                eachInvisible.setVisibility(View.GONE);
            }
        }
    }
}
